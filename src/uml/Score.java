package uml;

import java.io.FileInputStream;
import java.util.Enumeration;
import java.util.Properties;
import java.util.Scanner;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

public class Score {

	public static void main(String[] args) throws Exception{
		// TODO Auto-generated method stub
		Properties prop = new Properties();
		prop.load(new FileInputStream("src/uml/total.properties"));
		//读取tatal的内容
		Enumeration<?> fileName = prop.propertyNames();
		double a = Integer.parseInt(prop.getProperty("before"));
		double b = Integer.parseInt(prop.getProperty("base"));
		double c = Integer.parseInt(prop.getProperty("test"));
		double d = Integer.parseInt(prop.getProperty("program"));
		double e = Integer.parseInt(prop.getProperty("add"));
		//将所有total里的数字提取出来备用，等会计算会用到
		System.out.println(a);
		//这里是测试是否正确提取数字
		double my_a;
		double my_b;
		double my_c;
		double my_d;
		double my_e;
		
		try {
			String small_File;
			Document document = Jsoup.parse(small_File, "UTF-8");
			String all_File;
			Document document2 = Jsoup.parse(all_File, "UTF-8");
		
			// 对小班课上的五种经验值统分
			if (document != null) {

				// 将所有含有目标信息目标区域找出来
				Elements es = document.getElementsByAttributeValue("class", "interaction-row");
				int temp = 0;

				// 开始遍历各个目标区域并筛选数据
				for (int i = 0; i < es.size(); i++) {

					Element child = document.select("div[class=interaction-row]").get(i);

					if (child.select("span").get(1).toString().contains("课堂完成")) {
						if (child.toString().contains("已参与")) {
							Scanner sc = new Scanner(child.select("span").get(9).text());
							temp = sc.nextInt();
							my_b = my_b + temp;
						}
					} else if (child.select("span").get(1).toString().contains("课堂小测")) {
						if (child.toString().contains("已参与")) {
							Scanner sc = new Scanner(child.select("span").get(9).text());
							temp = sc.nextInt();
							my_c = my_c + temp;
							// 筛选互评分部分
							if (child.toString().contains("+")) {
								Scanner sc2 = new Scanner(child.select("span").get(11).text());
								temp = sc2.nextLine().toString().charAt(3) - 48;
								my_c = my_c + temp;
							}
						}
					} else if (child.select("span").get(1).toString().contains("编程题")) {
						if (child.select("span").get(8).text().contains("已参与")) {
							Scanner sc = new Scanner(child.select("span").get(9).text());
							temp = sc.nextInt();
							my_d = my_d + temp;
						}
					} else if (child.select("span").get(1).toString().contains("附加题")) {
						if (child.select("span").get(8).text().contains("已参与")) {
							Scanner sc = new Scanner(child.select("span").get(9).text());
							temp = sc.nextInt();
							my_e = my_e + temp;
						}
					} else {
						if (child.select("span").get(1).toString().contains("课前自测")) {
							if (child.toString().contains("color:#8FC31F;")) {
								Scanner sc = new Scanner(child.select("span").get(12).text());
								temp = sc.nextInt();
								my_a = my_a + temp;
							}
						}
					}
				}
			

			// 对大班课上的课前自测统分
			if (document2 != null) {
				Elements es2 = document2.getElementsByAttributeValue("class", "interaction-row");
				int temp2;
				for (int i = 0; i < es2.size(); i++)
					if (es2.get(i).select("span").get(1).toString().contains("课前自测")) {
						if (es2.get(i).toString().contains("color:#8FC31F")) {
							Scanner sc2 = new Scanner(es2.get(i).select("span").get(12).text());
							temp2 = sc2.nextInt();
							my_a = my_a + temp2;
						}
					}
			} 
				}
					}
				


	
		double my_all_a = my_a / a * 100 * 0.25;
		double my_all_b = my_b / b * 100 * 0.3 * 0.95;
		double my_all_c = my_c / c * 100 * 0.2;
		double my_all_d = my_d / d * 100 * 0.1;
		double my_all_e = my_e / e * 100 * 0.05;
		//计算各个阶段自己的得分
		double my_Score = (my_all_a+my_all_b+my_all_c+my_all_d+my_all_e) * 0.9 + 6;
		System.out.println(String.format("%.2f", my_Score));
		//得出总分数

		

	
	}}

